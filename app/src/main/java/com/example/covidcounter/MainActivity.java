package com.example.covidcounter;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.Dao.Api;
import com.Model.Country;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    List<Country> listCountries;
    Country c;
    LinearLayout countryCard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Retrofit retrofit= new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //initilize refrofit
        recyclerView = findViewById(R.id.articles);
        countryCard = findViewById(R.id.countryCard);
        listCountries = new ArrayList<>();
        //created api
        Api api = retrofit.create(Api.class);
        Call<List<Country>> call = api.getAllH();
        call.enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
                List<Country> lista = response.body();
                putDataInRecyclerView(lista);

            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {
                Log.i("onFailure",t.getMessage().toString());
            }
        });

    }
    private void putDataInRecyclerView(List<Country> list){
        com.Adapter.Adapter adapter= new com.Adapter.Adapter(this,list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }
}