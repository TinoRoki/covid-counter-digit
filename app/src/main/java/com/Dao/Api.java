package com.Dao;

import com.Model.Country;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {
    String BASE_URL = "https://api.coronatracker.com/v5/analytics/newcases/";
    @ GET("country?countryCode=MK&startDate=2020-10-12&endDate=2020-12-12") // smeni gi start i end date posle za od koga do koga sakas da se listat
    Call<List<Country>> getAllH();
}
