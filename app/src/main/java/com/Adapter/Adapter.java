package com.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.Model.Country;
import com.example.covidcounter.R;
import com.example.covidcounter.SecondActivity;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {
    private Context context;
    private List<Country> list;

    public Adapter(Context context, List<Country> list) {
        this.context = context;
        this.list = list;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        view = layoutInflater.inflate(R.layout.country_card,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.country_name.setText(list.get(position).getCountryName());
        holder.last_date.setText(list.get(position).getCountryCode());// ova ke go printat datumov
        holder.countryCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, SecondActivity.class);
                i.putExtra("next",list.get(position).getnInfections());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView country_name;
        TextView last_date;
        LinearLayout countryCard;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            country_name = itemView.findViewById(R.id.countryName); //
            last_date = itemView.findViewById(R.id.lastDate);
            countryCard = itemView.findViewById(R.id.countryCard);
        }
    }


}
