package com.Model;

import com.google.gson.annotations.SerializedName;

public class Country {
    @SerializedName("country")
    private String countryName;
    @SerializedName("country_code")
    private String countryCode;
    @SerializedName("last_updated")
    private String lUpdated;

    public Country(String countryName, String countryCode, String lUpdated, int nDeaths, int nInfections, int nCases) {
        this.countryName = countryName;
        this.countryCode = countryCode;
        this.lUpdated = lUpdated;
        this.nDeaths = nDeaths;
        this.nInfections = nInfections;
        this.nCases = nCases;
    }

    @SerializedName("new_deaths")
    private int nDeaths;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getlUpdated() {
        return lUpdated;
    }

    public void setlUpdated(String lUpdated) {
        this.lUpdated = lUpdated;
    }

    public int getnDeaths() {
        return nDeaths;
    }

    public void setnDeaths(int nDeaths) {
        this.nDeaths = nDeaths;
    }

    public int getnInfections() {
        return nInfections;
    }

    public void setnInfections(int nInfections) {
        this.nInfections = nInfections;
    }

    public int getnCases() {
        return nCases;
    }

    public void setnCases(int nCases) {
        this.nCases = nCases;
    }

    @SerializedName("new_infections")
    private int nInfections;
    @SerializedName("new_recovered")
    private int nCases;
}

